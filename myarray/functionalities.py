from myarray.basearray import BaseArray
import math

def print_array1d(base_array):
    if not len(base_array.shape) == 1:
        raise Exception(f'{base_array.shape} is not a valid shape')
    for c in range(1, base_array.shape[0]+1):
        print(c, end=" ")
    print('\n')


def print_array2d(base_array):
    if not len(base_array.shape) == 2:
        raise Exception(f'{base_array.shape} is not a valid shape')
    row, column = base_array.shape
    # get max sizes for all columns
    max_column_size_arr = []
    for c in range(1, column+1):
        max_size = 0
        for r in range(1, row+1):
            if len(str(base_array[r, c])) > max_size:
                max_size = len(str(base_array[r, c]))
        max_column_size_arr.append(max_size)
    # build output string
    output = "\n"
    for r in range(1, row+1):
        for c in range(1, column+1):
            output += ' ' * (max_column_size_arr[c-1] - len(str(base_array[r, c])))
            output += str(base_array[r, c])
            if not c == column:
                output += ' '
        if not r == row:
            output += '\n'
    print(output)


def print_array3d(base_array):
    if not len(base_array.shape) == 3:
        raise Exception(f'{base_array.shape} is not a valid shape')
    depth, row, column = base_array.shape
    # get max sizes for all columns
    max_column_size_arr = []
    for c in range(1, column+1):
        max_size = 0
        for d in range(1, depth+1):
            for r in range(1, row+1):
                if len(str(base_array[d, r, c])) > max_size:
                    max_size = len(str(base_array[d, r, c]))
        max_column_size_arr.append(max_size)
    # build output string
    output = "\n"
    for d in range(1, depth+1):
        for r in range(1, row+1):
            if r == 1:
                output += '['
            else:
                output += ' '
            for c in range(1, column+1):
                output += ' ' * (max_column_size_arr[c-1] - len(str(base_array[d, r, c])))
                output += str(base_array[d, r, c])
                if not c == column:
                    output += ' '
            if not r == row:
                output += '\n'
        output += ']'
        if not d == depth:
            output += '\n'
    print(output)


def _merge(left, right):
    result = []

    while left and right:
        if left[0] <= right[0]:
            result.append(left.pop(0))
        else:
            result.append(right.pop(0))

    result.extend(left)
    result.extend(right)
    return result


def _merge__sort(array):
    if len(array) <= 1:
        return array

    left = []
    right = []
    for i in range(len(array)):
        if i < len(array) / 2:
            left.append(array[i])
        else:
            right.append(array[i])

    left = _merge__sort(left)
    right = _merge__sort(right)

    return _merge(left, right)


def merge_sort(base_array, direction="horizontal"):
    result = []
    if len(base_array.shape) == 1:
        if direction != "horizontal":
            raise Exception(f'{direction} is not a valid sort direction')
        result = _merge__sort(list(base_array))
    elif len(base_array.shape) == 2:
        if direction == "horizontal":
            for row in range(base_array.shape[0]):
                result.extend(_merge__sort(
                    list(base_array)[row * base_array.shape[0]:row * base_array.shape[0] + base_array.shape[1]]))
        elif direction == "vertical":
            result = list(base_array)
            for column in range(base_array.shape[0]):
                result[column::base_array.shape[0]] = _merge__sort(result[column::base_array.shape[0]])
        else:
            # TODO: Exception
            pass
    else:
        # TODO: Exception
        pass
    return BaseArray(base_array.shape, data=result)


def search(base_array, value):
    result = []
    array = list(base_array)
    for i in range(len(array)):
        if array[i] == value:
            if len(base_array.shape) == 1:
                result.append((i+1,))
            elif len(base_array.shape) == 2:
                result.append((int(i / base_array.shape[1])+1,
                               int(i % base_array.shape[1])+1))
            elif len(base_array.shape) == 3:
                result.append((int(i / (base_array.shape[1] * base_array.shape[2])+1),
                               int((i % (base_array.shape[1] * base_array.shape[2])) / base_array.shape[2])+1,
                               int(i % base_array.shape[2])+1))
    return result


def math_functions(base_array, factor, funct, default_dtype=False):
    datatype = base_array.dtype
    if type(base_array) is not BaseArray:
        raise TypeError("First parameter is not of a type BaseArray")
    if type(factor) in [int, float]:
        if type(factor) is float or default_dtype:
            datatype = float
        return BaseArray(base_array.shape,
                         dtype=datatype,
                         data=[funct(i, factor) for i in base_array])
    elif type(factor) is BaseArray:
        if base_array.shape != factor.shape:
            raise ValueError("BaseArrays are not the same shape")
        if factor.dtype is float or default_dtype:
            datatype = float
        return BaseArray(base_array.shape,
                         dtype=datatype,
                         data=[funct(y, x) for y, x in zip(base_array, factor)])

    else:
        raise TypeError("Second parameter is not of a right type")


# Addition


def addition(base_array, factor):
    return math_functions(base_array, factor, lambda x, y: x + y)


# Subtraction


def subtraction(base_array, factor):
    return math_functions(base_array, factor, lambda x, y: x - y)


# Multiply


def multiply(base_array, factor):
    return math_functions(base_array, factor, lambda x, y: x * y)


# Divide


def divide(base_array, divident):
    return math_functions(base_array, divident, lambda x, y: x / y, True)


# TODO: Matrix multiply


def matrix_multiply(factor1, factor2):

    def dtype_decider(fa1, fa2):
        return float if fa1.dtype == float or fa2 == float else int

    if type(factor1) is not BaseArray:
        raise TypeError("First parameter is not a correct type. Expected type: BaseArray.")
    if type(factor2) is not BaseArray:
        raise TypeError("Second parameter is not a correct type. Expected type: BaseArray.")
    # 1D array with 1D array
    if len(factor1.shape) == 1 and len(factor2.shape) == 1:
        if factor1.shape[0] != factor2.shape[0]:
            raise ValueError("1D factors are different lengths.")
        out = 0
        for i in range(1, factor1.shape[0]+1):
            out += factor1[i]*factor2[i]
        return out
    # 2D array with 1D array
    elif len(factor1.shape) == 2 and len(factor2.shape) == 1:
        if factor1.shape[1] != factor2.shape[0]:
            raise ValueError("Factor shapes are not compatible.")
        out = []
        for x in range(1, factor1.shape[0]+1):
            sum = 0
            for xy in range(1, factor1.shape[1]+1):
                sum += factor1[x, xy]*factor2[xy]
            out.append(sum)
        if len(out) == 1:
            return out[0]
        else:
            return BaseArray((factor1.shape[0],),
                             dtype=dtype_decider(factor1, factor2),
                             data=out)
    # 1D array with 2D array
    elif len(factor1.shape) == 1 and len(factor2.shape) == 2:
        if factor1.shape[0] != factor2.shape[0]:
            raise ValueError("Factor shapes are not compatible.")
        out = []
        for y in range(1, factor2.shape[1]+1):
            sum = 0
            for xy in range(1, factor1.shape[0]+1):
                sum += factor1[xy] * factor2[xy,y]
            out.append(sum)
        if len(out) == 1:
            return out[0]
        else:
            return BaseArray((factor2.shape[1],),
                             dtype=dtype_decider(factor1, factor2),
                             data=out)
    # 2D array with 2D array
    elif len(factor1.shape) == 2 and len(factor2.shape) == 2:
        if factor1.shape[1] != factor2.shape[0]:
            raise ValueError("Factor shapes are not compatible.")
        out = []

        for x in range(1, factor1.shape[0]+1):
            for y in range(1, factor2.shape[1]+1):
                sum = 0
                for xy in range(1, factor1.shape[1]+1):
                    sum += factor1[x, xy] * factor2[xy, y]
                out.append(sum)

        if len(out) == 1:
            return out[0]
        else:
            return BaseArray((factor1.shape[0], factor2.shape[1]),
                             dtype=dtype_decider(factor1, factor2),
                             data=out)
    else:
        raise ValueError("One of factors is not a correct shape.")

# Logarithm


def logarithm(base_array, logarithm_factor):
    return math_functions(base_array, logarithm_factor, lambda x, y: math.log(x, y), True)


# Power


def power(base_array, power_factor):
    out_array = math_functions(base_array, power_factor, lambda x, y: math.pow(x, y))
    if out_array.dtype == int:
        out_array = BaseArray(out_array.shape,
                              dtype=out_array.dtype,
                              data=[int(x) for x in out_array])
    return out_array
