import cProfile
from myarray.basearray import BaseArray
import myarray.functionalities as func
import random
import pickle


filename = "list_of_rand.p"


def get_list_random(size):
    return random.sample(range(-size, size), size)


def write_list(itemlist, filename):
    with open(filename, 'wb') as fp:
        pickle.dump(itemlist, fp)


def read_list(filename):
    with open(filename, 'rb') as fp:
        itemlist = pickle.load(fp)
    return itemlist


#dt = get_list_random(100000)
#write_list(dt, filename)
dt = read_list(filename)

ba = BaseArray((100000,), dtype=int, data=dt)

cp = cProfile.Profile()
cp.enable()
func.merge_sort(ba)
cp.disable()
cp.print_stats()
cp.dump_stats("stats_file_after")
