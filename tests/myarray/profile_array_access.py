from myarray.basearray import BaseArray
import myarray.functionalities as func
import random


if __name__ == '__main__':
    a = BaseArray((1000, 1000), int)

    r_list = (random.randrange(0, 1000) for n in range(10000))
    c_list = (random.randrange(0, 1000) for n in range(10000))

    # access
    for r, c in zip(r_list, c_list):
        v = a[r, c]

    # set
    v = 1
    for r, c in zip(r_list, c_list):
        a[r, c] = v


