from unittest import TestCase
from myarray.basearray import BaseArray
import myarray.functionalities as func
import sys
import io


class TestPrint(TestCase):
    def test_print_array2d(self):
        # 1. test
        print_output = io.StringIO()
        sys.stdout = print_output
        func.print_array2d(BaseArray((2, 3), dtype=int))
        sys.stdout = sys.__stdout__
        self.assertEqual(print_output.getvalue(), "\n0 0 0\n0 0 0\n")
        # 2. test
        print_output = io.StringIO()
        input_array = [-1.32, -3.34, 3.,
                       3.432, 123., 5.,
                       23., 0., 1.]
        sys.stdout = print_output
        func.print_array2d(BaseArray((3, 3), dtype=float, data=input_array))
        sys.stdout = sys.__stdout__
        expected_string = "\n-1.32 -3.34 3.0\n" \
                          "3.432 123.0 5.0\n" \
                          " 23.0   0.0 1.0\n"
        self.assertEqual(print_output.getvalue(), expected_string)

    def test_print_array3d(self):
        # 1. test
        print_output = io.StringIO()
        sys.stdout = print_output
        func.print_array3d(BaseArray((3, 3, 3), dtype=int))
        sys.stdout = sys.__stdout__
        expected_string = "\n[0 0 0\n 0 0 0\n 0 0 0]\n" \
                          "[0 0 0\n 0 0 0\n 0 0 0]\n" \
                          "[0 0 0\n 0 0 0\n 0 0 0]\n"
        self.assertEqual(print_output.getvalue(), expected_string)
        # 2. test

        input_array = [-1.32, -3.34, 3.,
                       3.432, 123., 5.,
                       23., 0., 1.]
        print_output = io.StringIO()
        sys.stdout = print_output
        func.print_array3d(BaseArray((1, 3, 3), dtype=float, data=input_array))
        sys.stdout = sys.__stdout__
        expected_string = "\n[-1.32 -3.34 3.0\n" \
                          " 3.432 123.0 5.0\n" \
                          "  23.0   0.0 1.0]\n"
        self.assertEqual(print_output.getvalue(), expected_string)
        # 3. test
        input_array = [-1.0000001, 51., 86.3,
                       1., 1., 1.,
                       11., 11., 11.,
                       111., 111., 111.,
                       1111., 1111., 1111.,
                       11111., 11111., 11111.]
        print_output = io.StringIO()
        sys.stdout = print_output
        func.print_array3d(BaseArray((3, 2, 3), dtype=float, data=input_array))
        sys.stdout = sys.__stdout__
        expected_string = "\n[-1.0000001    51.0    86.3\n" \
                          "        1.0     1.0     1.0]\n" \
                          "[      11.0    11.0    11.0\n" \
                          "      111.0   111.0   111.0]\n" \
                          "[    1111.0  1111.0  1111.0\n" \
                          "    11111.0 11111.0 11111.0]\n"
        self.assertEqual(print_output.getvalue(), expected_string)


class TestSort(TestCase):
    def test_sort_1d(self):
        # 1. test
        input_array = [1.43, 2.35, -2.44, -2.35, 0.]
        expected_array = [-2.44, -2.35, 0., 1.43, 2.35]
        sorted_array = func.merge_sort(BaseArray((5,), data=input_array))
        expected_output = BaseArray((5,), data=expected_array)
        self.assertEqual(sorted_array.__dict__, expected_output.__dict__)

    def test_sort_2d(self):
        input_array = [-1.32, -3.34, 3.,
                       3.432, 123., 5.,
                       23., 0., 1.]
        expected_array_vertical = BaseArray((3, 3),
                                            dtype=float,
                                            data=[-1.32, -3.34, 1.,
                                                  3.432, 0., 3.,
                                                  23., 123., 5.])
        expected_array_horizontal = BaseArray((3, 3),
                                              dtype=float,
                                              data=[-3.34, -1.32, 3.,
                                                    3.432, 5., 123.,
                                                    0., 1., 23.])
        sorted_array_vertical = func.merge_sort(BaseArray((3, 3), dtype=float, data=input_array), "vertical")
        sorted_array_horizontal = func.merge_sort(BaseArray((3, 3), dtype=float, data=input_array), "horizontal")
        self.assertEqual(sorted_array_vertical.__dict__, expected_array_vertical.__dict__)
        self.assertEqual(sorted_array_horizontal.__dict__, expected_array_horizontal.__dict__)


class TestSearch(TestCase):
    def test_search_1d(self):
        input_array = [1, 0, 0, 3, 2, 5, 9, 0]
        self.assertEqual(func.search(BaseArray((8,), dtype=int, data=input_array), 0), [(2,), (3,), (8,)])
        input_array = [1.4, 0.1, 0.3, -3., 2., 5., 4.23, 0.1]
        self.assertEqual(func.search(BaseArray((8,), dtype=float, data=input_array), 0.1), [(2,), (8,)])
        self.assertEqual(func.search(BaseArray((8,), dtype=float, data=input_array), -5.321), [])

    def test_search_2d(self):
        input_array = [0, 0, 0,
                       2, 0, 0,
                       0, 2, 2]
        self.assertEqual(func.search(BaseArray((3, 3), dtype=int, data=input_array), 2), [(2, 1), (3, 2), (3, 3)])
        input_array = [-1.32, -3.34,
                       3.432, 123.,
                       123., 0.]
        self.assertEqual(func.search(BaseArray((3, 2), dtype=float, data=input_array), 123.), [(2, 2), (3, 1)])
        self.assertEqual(func.search(BaseArray((3, 2), dtype=float, data=input_array), 6548.), [])

    def test_search_3d(self):
        input_array = [0, 5, 0, 0,
                       0, 0, 0, 5,
                       0, 0, 0, 0,

                       5, 0, 0, 0,
                       0, 5, 0, 0,
                       0, 0, 0, 0,

                       0, 0, 0, 0,
                       0, 0, 5, 5,
                       0, 5, 0, 5]
        expected_indices = [(1, 1, 2),
                            (1, 2, 4),
                            (2, 1, 1),
                            (2, 2, 2),
                            (3, 2, 3),
                            (3, 2, 4),
                            (3, 3, 2),
                            (3, 3, 4)]
        self.assertEqual(func.search(BaseArray((3, 3, 4), dtype=int, data=input_array), 5), expected_indices)
        self.assertEqual(func.search(BaseArray((3, 3, 4), dtype=int, data=input_array), 3), [])


class TestMathematicOperations(TestCase):

    # Addition

    def test_addition_1d_int(self):
        suma_array = func.addition(BaseArray((6,),
                                             dtype=int,
                                             data=[0, 1, 2, 3, 4, 5]),
                                   4)
        expected_array = BaseArray((6,),
                                   dtype=int,
                                   data=[4, 5, 6, 7, 8, 9])

        self.assertEqual(suma_array.__dict__, expected_array.__dict__)

        suma_array = func.addition(BaseArray((6,),
                                             dtype=float,
                                             data=[0., 1., 2., 3., 4., 5.]),
                                   4)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[4., 5., 6., 7., 8., 9.])

        self.assertEqual(suma_array.__dict__, expected_array.__dict__)

        suma_array = func.addition(BaseArray((6,),
                                             dtype=int,
                                             data=[0, 1, 2, 3, 4, 5]),
                                   4.0)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[4., 5., 6., 7., 8., 9.])

        self.assertEqual(suma_array.__dict__, expected_array.__dict__)

    def test_addition_2d_int(self):
        suma_array = func.addition(BaseArray((2, 3),
                                             dtype=int,
                                             data=[0, 1, 2,
                                                   3, 4, 5]),
                                   4)
        expected_array = BaseArray((2, 3),
                                   dtype=int,
                                   data=[4, 5, 6,
                                         7, 8, 9])

        self.assertEqual(suma_array.__dict__, expected_array.__dict__)

    def test_addition_3d_int(self):
        suma_array = func.addition(BaseArray((2, 2, 3),
                                             dtype=int,
                                             data=[0, 1, 2,
                                                   3, 4, 5,

                                                   6, 7, 8,
                                                   9, 10, 11]),
                                   4)
        expected_array = BaseArray((2, 2, 3),
                                   dtype=int,
                                   data=[4, 5, 6,
                                         7, 8, 9,

                                         10, 11, 12,
                                         13, 14, 15])

        self.assertEqual(suma_array.__dict__, expected_array.__dict__)

    def test_addition_1d_1d(self):
        suma_array = func.addition(BaseArray((6,),
                                             dtype=int,
                                             data=[0, 1, 2, 3, 4, 5]),
                                   BaseArray((6,),
                                             dtype=float,
                                             data=[1., 2., 1., 2., 1., 2.]))
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[1., 3., 3., 5., 5., 7.])

        self.assertEqual(suma_array.__dict__, expected_array.__dict__)

    def test_addition_2d_2d(self):
        suma_array = func.addition(BaseArray((2, 3),
                                             dtype=int,
                                             data=[0, 1, 2,
                                                   3, 4, 5]),
                                   BaseArray((2, 3),
                                             dtype=float,
                                             data=[1., 2., 1.,
                                                   2., 1., 2.]))
        expected_array = BaseArray((2, 3),
                                   dtype=float,
                                   data=[1., 3., 3.,
                                         5., 5., 7.])
        self.assertEqual(suma_array.__dict__, expected_array.__dict__)

    def test_addition_3d_3d(self):
        suma_array = func.addition(BaseArray((2, 2, 3),
                                             dtype=int,
                                             data=[0, 1, 2,
                                                   3, 4, 5,

                                                   6, 7, 8,
                                                   9, 10, 11]),
                                   BaseArray((2, 2, 3),
                                             dtype=int,
                                             data=[4, 4, 4,
                                                   4, 4, 4,

                                                   4, 4, 4,
                                                   4, 4, 4]))
        expected_array = BaseArray((2, 2, 3),
                                   dtype=int,
                                   data=[4, 5, 6,
                                         7, 8, 9,

                                         10, 11, 12,
                                         13, 14, 15])
        self.assertEqual(suma_array.__dict__, expected_array.__dict__)

    def test_addition_3d_2d(self):
        with self.assertRaises(ValueError):
            func.addition(BaseArray((2, 2, 3),
                                    dtype=int,
                                    data=[0, 1, 2,
                                          3, 4, 5,

                                          6, 7, 8,
                                          9, 10, 11]),
                          BaseArray((2, 3),
                                    dtype=int,
                                    data=[4, 4, 4,
                                          4, 4, 4]))

    # Subtract

    def test_subtraction_1d_int(self):
        test_array = func.subtraction(BaseArray((6,),
                                                dtype=int,
                                                data=[0, 1, 2, 3, 4, 5]),
                                      4)
        expected_array = BaseArray((6,),
                                   dtype=int,
                                   data=[-4, -3, -2, -1, -0, 1])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

        test_array = func.subtraction(BaseArray((6,),
                                                dtype=float,
                                                data=[0., 1., 2., 3., 4., 5.]),
                                      4)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[-4., -3., -2., -1., 0., 1.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

        test_array = func.subtraction(BaseArray((6,),
                                                dtype=int,
                                                data=[0, 1, 2, 3, 4, 5]),
                                      4.0)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[-4., -3., -2., -1., 0, 1])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_subtraction_2d_int(self):
        test_array = func.subtraction(BaseArray((2, 3),
                                                dtype=int,
                                                data=[0, 1, 2,
                                                      3, 4, 5]),
                                      4)
        expected_array = BaseArray((2, 3),
                                   dtype=int,
                                   data=[-4, -3, -2,
                                         -1, 0, 1])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_subtraction_3d_int(self):
        test_array = func.subtraction(BaseArray((2, 2, 3),
                                                dtype=int,
                                                data=[0, 1, 2,
                                                      3, 4, 5,

                                                      6, 7, 8,
                                                      9, 10, 11]),
                                      4)
        expected_array = BaseArray((2, 2, 3),
                                   dtype=int,
                                   data=[-4, -3, -2,
                                         -1, 0, 1,

                                         2, 3, 4,
                                         5, 6, 7])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_subtraction_1d_1d(self):
        test_array = func.subtraction(BaseArray((6,),
                                                dtype=int,
                                                data=[0, 1, 2, 3, 4, 5]),
                                      BaseArray((6,),
                                                dtype=float,
                                                data=[1., 2., 1., 2., 1., 2.]))
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[-1., -1., 1., 1., 3., 3.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_subtraction_2d_2d(self):
        test_array = func.subtraction(BaseArray((2, 3),
                                                dtype=int,
                                                data=[0, 1, 2,
                                                      3, 4, 5]),
                                      BaseArray((2, 3),
                                                dtype=float,
                                                data=[1., 2., 1.,
                                                      2., 1., 2.]))
        expected_array = BaseArray((2, 3),
                                   dtype=float,
                                   data=[-1., -1., 1.,
                                         1., 3., 3.])
        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_subtraction_3d_3d(self):
        test_array = func.subtraction(BaseArray((2, 2, 3),
                                                dtype=int,
                                                data=[0, 1, 2,
                                                      3, 4, 5,

                                                      6, 7, 8,
                                                      9, 10, 11]),
                                      BaseArray((2, 2, 3),
                                                dtype=int,
                                                data=[4, 4, 4,
                                                      4, 4, 4,

                                                      2, 2, 2,
                                                      2, 2, 2]))
        expected_array = BaseArray((2, 2, 3),
                                   dtype=int,
                                   data=[-4, -3, -2,
                                         -1, 0, 1,

                                         4, 5, 6,
                                         7, 8, 9])
        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_subtraction_3d_2d(self):
        with self.assertRaises(ValueError):
            func.subtraction(BaseArray((2, 2, 3),
                                       dtype=int,
                                       data=[0, 1, 2,
                                             3, 4, 5,

                                             6, 7, 8,
                                             9, 10, 11]),
                             BaseArray((2, 3),
                                       dtype=int,
                                       data=[4, 4, 4,
                                             4, 4, 4]))

    # Multiply

    def test_multiplication_1d_int(self):
        mul_array = func.multiply(BaseArray((6,),
                                            dtype=int,
                                            data=[0, 1, 2, 3, 4, 5]),
                                  4)
        expected_array = BaseArray((6,),
                                   dtype=int,
                                   data=[0, 4, 8, 12, 16, 20])

        self.assertEqual(mul_array.__dict__, expected_array.__dict__)

        mul_array = func.multiply(BaseArray((6,),
                                            dtype=float,
                                            data=[0., 1., 2., 3., 4., 5.]),
                                  4)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[0., 4., 8., 12., 16., 20.])

        self.assertEqual(mul_array.__dict__, expected_array.__dict__)

        mul_array = func.multiply(BaseArray((6,),
                                            dtype=int,
                                            data=[0, 1, 2, 3, 4, 5]),
                                  -4.0)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[0., -4., -8., -12., -16., -20.])

        self.assertEqual(mul_array.__dict__, expected_array.__dict__)

    def test_multiplication_2d_int(self):
        mul_array = func.multiply(BaseArray((2, 3),
                                            dtype=int,
                                            data=[0, 1, 2,
                                                  3, 4, 5]),
                                  4)
        expected_array = BaseArray((2, 3),
                                   dtype=int,
                                   data=[0, 4, 8,
                                         12, 16, 20])

        self.assertEqual(mul_array.__dict__, expected_array.__dict__)

    def test_multiplication_3d_int(self):
        mul_array = func.multiply(BaseArray((2, 2, 3),
                                            dtype=int,
                                            data=[0, 1, 2,
                                                  3, 4, 5,

                                                  6, 7, 8,
                                                  9, 10, 11]),
                                  2)
        expected_array = BaseArray((2, 2, 3),
                                   dtype=int,
                                   data=[0, 2, 4,
                                         6, 8, 10,

                                         12, 14, 16,
                                         18, 20, 22])

        self.assertEqual(mul_array.__dict__, expected_array.__dict__)

    def test_multiplication_1d_1d(self):
        mul_array = func.multiply(BaseArray((6,),
                                            dtype=int,
                                            data=[0, 1, 2, 3, 4, 5]),
                                  BaseArray((6,),
                                            dtype=float,
                                            data=[1., 2., 1., 2., 1., 2.]))
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[0., 2., 2., 6., 4., 10.])

        self.assertEqual(mul_array.__dict__, expected_array.__dict__)

    def test_multiplication_2d_2d(self):
        mul_array = func.multiply(BaseArray((2, 3),
                                            dtype=int,
                                            data=[0, 1, 2,
                                                  3, 4, 5]),
                                  BaseArray((2, 3),
                                            dtype=float,
                                            data=[1., 2., 1.,
                                                  2., 1., 2.]))
        expected_array = BaseArray((2, 3),
                                   dtype=float,
                                   data=[0., 2., 2.,
                                         6., 4., 10.])
        self.assertEqual(mul_array.__dict__, expected_array.__dict__)

    def test_multiplication_3d_3d(self):
        mul_array = func.multiply(BaseArray((2, 2, 3),
                                            dtype=int,
                                            data=[0, 1, 2,
                                                  3, 4, 5,

                                                  6, 7, 8,
                                                  9, 10, 11]),
                                  BaseArray((2, 2, 3),
                                            dtype=int,
                                            data=[4, 4, 4,
                                                  4, 4, 4,

                                                  4, 4, 4,
                                                  4, 4, 4]))
        expected_array = BaseArray((2, 2, 3),
                                   dtype=int,
                                   data=[0, 4, 8,
                                         12, 16, 20,

                                         24, 28, 32,
                                         36, 40, 44])
        self.assertEqual(mul_array.__dict__, expected_array.__dict__)

    def test_multiplication_3d_2d(self):
        with self.assertRaises(ValueError):
            func.multiply(BaseArray((2, 2, 3),
                                    dtype=int,
                                    data=[0, 1, 2,
                                          3, 4, 5,

                                          6, 7, 8,
                                          9, 10, 11]),
                          BaseArray((2, 3),
                                    dtype=int,
                                    data=[4, 4, 4,
                                          4, 4, 4]))

    # Division

    def test_division_1d_int(self):
        test_array = func.divide(BaseArray((6,),
                                           dtype=int,
                                           data=[0, 4, 8, 12, 16, 20]),
                                 4)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[0., 1., 2., 3., 4., 5.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

        test_array = func.divide(BaseArray((6,),
                                           dtype=float,
                                           data=[0., 4., 8., 12., 16., 20.]),
                                 4)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[0., 1., 2., 3., 4., 5.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

        test_array = func.divide(BaseArray((6,),
                                           dtype=int,
                                           data=[0, -4, -8, -12, -16, -20]),
                                 -4.0)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[0., 1., 2., 3., 4., 5.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_division_2d_int(self):
        test_array = func.divide(BaseArray((2, 3),
                                           dtype=int,
                                           data=[0, 4, 8,
                                                 12, 16, 20]),
                                 4)
        expected_array = BaseArray((2, 3),
                                   dtype=float,
                                   data=[0., 1., 2.,
                                         3., 4., 5.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_division_3d_int(self):
        test_array = func.divide(BaseArray((2, 2, 3),
                                           dtype=int,
                                           data=[0, 2, 4,
                                                 6, 8, 10,

                                                 12, 14, 16,
                                                 18, 20, 22]),
                                 2)
        expected_array = BaseArray((2, 2, 3),
                                   dtype=float,
                                   data=[0., 1., 2.,
                                         3., 4., 5.,

                                         6., 7., 8.,
                                         9., 10., 11.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_division_1d_1d(self):
        test_array = func.divide(BaseArray((6,),
                                           dtype=float,
                                           data=[0., 2., 2., 6., 4., 10.]),
                                 BaseArray((6,),
                                           dtype=float,
                                           data=[1., 2., 1., 2., 1., 2.]))
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[0., 1., 2., 3., 4., 5.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_division_2d_2d(self):
        test_array = func.divide(BaseArray((2, 3),
                                           dtype=float,
                                           data=[0., 2., 2.,
                                                 6., 4., 10.]),
                                 BaseArray((2, 3),
                                           dtype=float,
                                           data=[1., 2., 1.,
                                                 2., 1., 2.]))
        expected_array = BaseArray((2, 3),
                                   dtype=float,
                                   data=[0., 1., 2.,
                                         3., 4., 5.])
        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_division_3d_3d(self):
        test_array = func.divide(BaseArray((2, 2, 3),
                                           dtype=int,
                                           data=[0, 4, 8,
                                                 12, 16, 20,

                                                 24, 28, 32,
                                                 36, 40, 44]),
                                 BaseArray((2, 2, 3),
                                           dtype=int,
                                           data=[4, 4, 4,
                                                 4, 4, 4,

                                                 4, 4, 4,
                                                 4, 4, 4]))
        expected_array = BaseArray((2, 2, 3),
                                   dtype=float,
                                   data=[0., 1., 2.,
                                         3., 4., 5.,

                                         6., 7., 8.,
                                         9., 10., 11.])
        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_division_3d_2d(self):
        with self.assertRaises(ValueError):
            func.divide(BaseArray((2, 2, 3),
                                  dtype=int,
                                  data=[0, 1, 2,
                                        3, 4, 5,

                                        6, 7, 8,
                                        9, 10, 11]),
                        BaseArray((2, 3),
                                  dtype=int,
                                  data=[4, 4, 4,
                                        4, 4, 4]))

    def test_division_by_zero(self):
        with self.assertRaises(ZeroDivisionError):
            func.divide(BaseArray((3,),
                                  dtype=int,
                                  data=[1, 2, 3]),
                        0)
        with self.assertRaises(ZeroDivisionError):
            func.divide(BaseArray((3,),
                                  dtype=int,
                                  data=[1, 2, 3]),
                        BaseArray((3,),
                                  dtype=int,
                                  data=[1, 0, 1]))

    # Matrix multiply

    def test_matrix_multiply_scalar(self):
        with self.assertRaises(TypeError):
            func.matrix_multiply(BaseArray((2, 2, 3),
                                           dtype=int,
                                           data=[0, 1, 2,
                                                 3, 4, 5,

                                                 6, 7, 8,
                                                 9, 10, 11]),
                                 3)

    def test_matrix_multiply_1D(self):
        test_value = func.matrix_multiply(BaseArray((3,),
                                                    dtype=int,
                                                    data=[0, 1, 3]),
                                          BaseArray((3,),
                                                    dtype=float,
                                                    data=[1., 2., 1.]))

        self.assertEqual(test_value, 5.)

    def test_matrix_multiply_1D_2D(self):
        test_array = func.matrix_multiply(BaseArray((3,),
                                                    dtype=float,
                                                    data=[0., 2., 2.]),
                                          BaseArray((3, 2),
                                                    dtype=float,
                                                    data=[1., 2.,
                                                          1., 2.,
                                                          1., 2.]))
        expected_array = BaseArray((2, ),
                                   dtype=float,
                                   data=[4., 8.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_matrix_multiply_2D(self):
        test_array = func.matrix_multiply(BaseArray((2, 3),
                                                    dtype=float,
                                                    data=[0., 2., 2.,
                                                          6., 4., 10.]),
                                          BaseArray((3, 2),
                                                    dtype=float,
                                                    data=[1., 2.,
                                                          1., 2.,
                                                          1., 2.]))
        expected_array = BaseArray((2, 2),
                                   dtype=float,
                                   data=[4., 8.,
                                         20., 40.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

        test_value = func.matrix_multiply(BaseArray((6,),
                                                    dtype=float,
                                                    data=[0., 2., 2., 6., 4., 10.]),
                                          BaseArray((6, 1),
                                                    dtype=float,
                                                    data=[1.,
                                                          2.,
                                                          1.,
                                                          2.,
                                                          1.,
                                                          2.]))

        self.assertEqual(test_value, 42.)

        test_value = func.matrix_multiply(BaseArray((1, 6),
                                                    dtype=float,
                                                    data=[0., 2., 2., 6., 4., 10.]),
                                          BaseArray((6, 1),
                                                    dtype=float,
                                                    data=[1.,
                                                          2.,
                                                          1.,
                                                          2.,
                                                          1.,
                                                          2.]))

        self.assertEqual(test_value, 42.)

    def test_matrix_multiply_different_shapes(self):
        with self.assertRaises(ValueError):
            func.matrix_multiply(BaseArray((2, 3),
                                           dtype=int,
                                           data=[0, 1, 2,
                                                 3, 4, 5]),
                                 BaseArray((2, 3),
                                           dtype=int,
                                           data=[4, 4, 4,
                                                 4, 4, 4]))

    # Logarithm

    def test_logarithm_1d_int(self):
        test_array = func.logarithm(BaseArray((6,),
                                              dtype=int,
                                              data=[1, 2, 4, 8, 16, 32]),
                                    2)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[0., 1., 2., 3., 4., 5.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

        test_array = func.logarithm(BaseArray((6,),
                                              dtype=float,
                                              data=[1., 2., 4., 8., 16., 32.]),
                                    2)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[0., 1., 2., 3., 4., 5.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

        test_array = func.logarithm(BaseArray((6,),
                                              dtype=int,
                                              data=[1, 2, 4, 8, 16, 32]),
                                    2.0)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[0., 1., 2., 3., 4., 5.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_logarithm_2d_int(self):
        test_array = func.logarithm(BaseArray((2, 3),
                                              dtype=int,
                                              data=[1, 2, 4,
                                                    8, 16, 32]),
                                    2)
        expected_array = BaseArray((2, 3),
                                   dtype=float,
                                   data=[0., 1., 2.,
                                         3., 4., 5.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_logarithm_3d_int(self):
        test_array = func.logarithm(BaseArray((2, 2, 3),
                                              dtype=int,
                                              data=[1, 2, 4,
                                                    8, 16, 32,

                                                    64, 128, 256,
                                                    512, 1024, 2048]),
                                    2)
        expected_array = BaseArray((2, 2, 3),
                                   dtype=float,
                                   data=[0., 1., 2.,
                                         3., 4., 5.,

                                         6., 7., 8.,
                                         9., 10., 11.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_logarithm_1d_1d(self):
        test_array = func.logarithm(BaseArray((6,),
                                              dtype=float,
                                              data=[27., 3., 9., 8., 4., 16.]),
                                    BaseArray((6,),
                                              dtype=float,
                                              data=[3., 3., 3., 2., 2., 2.]))
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[3., 1., 2., 3., 2., 4.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_logarithm_2d_2d(self):
        test_array = func.logarithm(BaseArray((2, 3),
                                              dtype=float,
                                              data=[27., 3., 9.,
                                                    8., 4., 16.]),
                                    BaseArray((2, 3),
                                              dtype=float,
                                              data=[3., 3., 3.,
                                                    2., 2., 2.]))
        expected_array = BaseArray((2, 3),
                                   dtype=float,
                                   data=[3., 1., 2.,
                                         3., 2., 4.])
        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_logarithm_3d_3d(self):
        test_array = func.logarithm(BaseArray((2, 2, 3),
                                              dtype=int,
                                              data=[1, 4, 1,
                                                    16, 16, 16,

                                                    4, 4, 4,
                                                    1, 1, 1]),
                                    BaseArray((2, 2, 3),
                                              dtype=int,
                                              data=[4, 4, 4,
                                                    4, 4, 4,

                                                    4, 4, 2,
                                                    4, 4, 2]))
        expected_array = BaseArray((2, 2, 3),
                                   dtype=float,
                                   data=[0., 1., 0.,
                                         2., 2., 2.,

                                         1., 1., 2.,
                                         0., 0., 0.])
        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_logarithm_3d_2d(self):
        with self.assertRaises(ValueError):
            func.logarithm(BaseArray((2, 2, 3),
                                     dtype=int,
                                     data=[3, 2, 2,
                                           3, 4, 5,

                                           6, 7, 8,
                                           9, 10, 11]),
                           BaseArray((2, 3),
                                     dtype=int,
                                     data=[4, 4, 4,
                                           4, 4, 4]))

    def test_logarithm_of_zero_value(self):
        with self.assertRaises(ValueError):
            func.logarithm(BaseArray((3,),
                                     dtype=int,
                                     data=[2, 2, 0]),
                           2)

    def test_logarithm_with_base_zero(self):
        with self.assertRaises(ValueError):
            func.logarithm(BaseArray((3,),
                                     dtype=int,
                                     data=[2, 2, 3]),
                           0)
        with self.assertRaises(ValueError):
            func.logarithm(BaseArray((3,),
                                     dtype=int,
                                     data=[1, 2, 3]),
                           BaseArray((3,),
                                     dtype=int,
                                     data=[3, 0, 2]))

    def test_logarithm_with_negative_value(self):
        with self.assertRaises(ValueError):
            func.logarithm(BaseArray((3,),
                                     dtype=int,
                                     data=[1, -2, 3]),
                           3)

    def test_logarithm_with_negative_base(self):
        with self.assertRaises(ValueError):
            func.logarithm(BaseArray((3,),
                                     dtype=int,
                                     data=[1, 2, 3]),
                           -3)

    def test_logarithm_with_base_one(self):
        with self.assertRaises(ZeroDivisionError):
            func.logarithm(BaseArray((3,),
                                     dtype=int,
                                     data=[1, 2, 3]),
                           1)
        with self.assertRaises(ZeroDivisionError):
            func.logarithm(BaseArray((3,),
                                     dtype=int,
                                     data=[1, 2, 3]),
                           BaseArray((3,),
                                     dtype=int,
                                     data=[1, 4, 1]))

    # Power

    def test_power_1d_int(self):
        test_array = func.power(BaseArray((6,),
                                          dtype=int,
                                          data=[0, 1, 2, 3, 4, 5]),
                                2)
        expected_array = BaseArray((6,),
                                   dtype=int,
                                   data=[0, 1, 4, 9, 16, 25])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

        test_array = func.power(BaseArray((6,),
                                          dtype=float,
                                          data=[0., 1., 2., 3., 4., 5.]),
                                2)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[0., 1., 4., 9., 16., 25.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

        test_array = func.power(BaseArray((6,),
                                          dtype=int,
                                          data=[0, 1, 2, 3, 4, 5]),
                                2.0)
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[0., 1., 4., 9., 16., 25.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_power_2d_int(self):
        test_array = func.power(BaseArray((2, 3),
                                          dtype=int,
                                          data=[0, 1, 2,
                                                3, 4, 5]),
                                2)
        expected_array = BaseArray((2, 3),
                                   dtype=int,
                                   data=[0, 1, 4,
                                         9, 16, 25])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_power_3d_int(self):
        test_array = func.power(BaseArray((2, 2, 3),
                                          dtype=int,
                                          data=[1, 2, 3,
                                                4, 5, 6,

                                                7, 8, 9,
                                                10, 11, 12]),
                                2)
        expected_array = BaseArray((2, 2, 3),
                                   dtype=int,
                                   data=[1, 4, 9,
                                         16, 25, 36,

                                         49, 64, 81,
                                         100, 121, 144])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_power_1d_1d(self):
        test_array = func.power(BaseArray((6,),
                                          dtype=float,
                                          data=[1., 3., 2., 6., 4., 10.]),
                                BaseArray((6,),
                                          dtype=float,
                                          data=[1., 3., 3., 2., 2., 2.]))
        expected_array = BaseArray((6,),
                                   dtype=float,
                                   data=[1., 27., 8., 36., 16., 100.])

        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_power_2d_2d(self):
        test_array = func.power(BaseArray((2, 3),
                                          dtype=float,
                                          data=[1., 3., 4.,
                                                8., 4., 10.]),
                                BaseArray((2, 3),
                                          dtype=float,
                                          data=[1., 3., 3.,
                                                2., 2., 2.]))
        expected_array = BaseArray((2, 3),
                                   dtype=float,
                                   data=[1., 27., 64.,
                                         64., 16., 100.])
        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_power_3d_3d(self):
        test_array = func.power(BaseArray((2, 2, 3),
                                          dtype=int,
                                          data=[1, 2, 1,
                                                2, 3, 2,

                                                2, 3, 2,
                                                1, 2, 1]),
                                BaseArray((2, 2, 3),
                                          dtype=int,
                                          data=[2, 2, 2,
                                                2, 3, 2,

                                                2, 3, 2,
                                                2, 2, 2]))
        expected_array = BaseArray((2, 2, 3),
                                   dtype=int,
                                   data=[1, 4, 1,
                                         4, 27, 4,

                                         4, 27, 4,
                                         1, 4, 1])
        self.assertEqual(test_array.__dict__, expected_array.__dict__)

    def test_power_3d_2d(self):
        with self.assertRaises(ValueError):
            func.power(BaseArray((2, 2, 3),
                                 dtype=int,
                                 data=[0, 1, 2,
                                       3, 4, 5,

                                       6, 7, 8,
                                       9, 10, 11]),
                       BaseArray((2, 3),
                                 dtype=int,
                                 data=[4, 4, 4,
                                       4, 4, 4]))
