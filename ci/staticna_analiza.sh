#!/usr/bin/env bash

pylint myarray/functionalities.py

EXIT_VAR=$?

echo "Exit status: $EXIT_VAR"
if (( ($EXIT_VAR&3 ) == 0 )); then
    echo "Script returns 0"
    exit 0
fi

echo "Script returns $EXIT_VAR"
exit $EXIT_VAR